package com.pms.sdk;

import java.io.Serializable;

import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.pms.sdk.api.APIManager.APICallback;
import com.pms.sdk.api.request.SetConfig;
import com.pms.sdk.bean.Msg;
import com.pms.sdk.bean.MsgGrp;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.DataKeyUtil;
import com.pms.sdk.common.util.PMSUtil;
import com.pms.sdk.common.util.StringUtil;
import com.pms.sdk.db.PMSDB;

public class PMS implements IPMSConsts, Serializable {

	private static final long serialVersionUID = 1L;

	private static PMS instancePms = null;

	private static PMSPopup instancePmsPopup = null;

	private Context mContext = null;

	private PMSDB mDB = null;

	private PMS(Context context) {
		this.mDB = PMSDB.getInstance(context);
		CLog.i("Version:" + PMS_VERSION + ",UpdateDate:"+PMS_VERSION_UPDATE_DATE);

		initOption(context);
	}

	public static PMS getInstance (Context context) {
		CLog.d("getInstance:projectId=" + PMSUtil.getGCMProjectId(context));
		CLog.setDebugMode(context);
		CLog.setDebugName(context);
		if (instancePms == null) {
			instancePms = new PMS(context);
		}
		instancePms.setmContext(context);
		return instancePms;
	}

	public static PMSPopup getPopUpInstance () {
		return instancePmsPopup;
	}

	public static boolean clear () {
		try {
			PMSUtil.setDeviceCertStatus(instancePms.mContext, DEVICECERT_PENDING);
			instancePms = null;
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	private void initOption (Context context) {
		if (StringUtil.isEmpty(DataKeyUtil.getDBKey(context, DB_RING_FLAG))) {
			DataKeyUtil.setDBKey(context, DB_RING_FLAG, "Y");
		}
		if (StringUtil.isEmpty(DataKeyUtil.getDBKey(context, DB_VIBE_FLAG))) {
			DataKeyUtil.setDBKey(context, DB_VIBE_FLAG, "Y");
		}
		if (StringUtil.isEmpty(DataKeyUtil.getDBKey(context, DB_ALERT_FLAG))) {
			DataKeyUtil.setDBKey(context, DB_ALERT_FLAG, "N");
		}
		if (StringUtil.isEmpty(DataKeyUtil.getDBKey(context, DB_MAX_USER_MSG_ID))) {
			DataKeyUtil.setDBKey(context, DB_MAX_USER_MSG_ID, "-1");
		}
	}

	public void setCustId (String custId) {
		PMSUtil.setCustId(mContext, custId);
	}

	public String getCustId () {
		return PMSUtil.getCustId(mContext);
	}

	private void setmContext (Context context) {
		this.mContext = context;
	}

	public void setPopupSetting (Boolean state, String title) {
		instancePmsPopup = PMSPopup.getInstance(mContext, mContext.getPackageName(), state, title);
	}

	public void setIsPopupActivity (Boolean ispopup) {
		PMSUtil.setPopupActivity(mContext, ispopup);
	}

	public void setNotiOrPopup (Boolean isnotiorpopup) {
		PMSUtil.setNotiOrPopup(mContext, isnotiorpopup);
	}

	public void setRingMode (boolean isRingMode) {
		DataKeyUtil.setDBKey(mContext, DB_RING_FLAG, isRingMode ? "Y" : "N");
	}

	public void setVibeMode (boolean isVibeMode) {
		DataKeyUtil.setDBKey(mContext, DB_VIBE_FLAG, isVibeMode ? "Y" : "N");
	}

	public void setPopupNoti (boolean isShowPopup) {
		DataKeyUtil.setDBKey(mContext, DB_ALERT_FLAG, isShowPopup ? "Y" : "N");
	}

	public void setConfig (String msgFlag, String notiFlag, String mktFlag, String personFlag, String umsFlag, final APICallback apiCallback) {
		new SetConfig(mContext).request(msgFlag, notiFlag, mktFlag, personFlag, umsFlag, new APICallback() {
			@Override
			public void response (String code, JSONObject json) {
				if (apiCallback != null) {
					apiCallback.response(code, json);
				}
			}
		});
	}

	public String getMsgFlag () {
		return DataKeyUtil.getDBKey(mContext, DB_MSG_FLAG);
	}

	public String getNotiFlag () {
		return DataKeyUtil.getDBKey(mContext, DB_NOTI_FLAG);
	}

	public String getMkFlag () {
		return DataKeyUtil.getDBKey(mContext, DB_MKT_FLAG);
	}

	public String getMaxUserMsgId () {
		return DataKeyUtil.getDBKey(mContext, DB_MAX_USER_MSG_ID);
	}

	public String getDupFlag () {
		return DataKeyUtil.getDBKey(mContext, DB_DUP_FLAG);
	}

	public String getCustIdCom () {
		return DataKeyUtil.getDBKey(mContext, DB_CUST_ID_COM);
	}

	public void setNotiBigStyle(boolean isUse)
	{
		DataKeyUtil.setDBKey(mContext, DB_NOTI_BIG_TEXT_STYLE, (isUse ? "Y" : "N"));
	}

	/*
	 * ===================================================== [start] database =====================================================
	 */
	/**
	 * select MsgGrp list
	 * 
	 * @return
	 */
	public Cursor selectMsgGrpList () {
		return mDB.selectMsgGrpList();
	}

	/**
	 * select MsgGrp
	 * 
	 * @param msgCode
	 * @return
	 */
	public MsgGrp selectMsGrp (String msgCode) {
		return mDB.selectMsgGrp(msgCode);
	}

	/**
	 * select new msg Cnt
	 * 
	 * @return
	 */
	public int selectNewMsgCnt () {
		return mDB.selectNewMsgCnt();
	}

	/**
	 * select Msg List
	 * 
	 * @param page
	 * @param row
	 * @return
	 */
	public Cursor selectMsgList (int page, int row) {
		return mDB.selectMsgList(page, row);
	}

	/**
	 * select Msg List
	 * 
	 * @param msgCode
	 * @return
	 */
	public Cursor selectMsgList (String msgCode) {
		return mDB.selectMsgList(msgCode);
	}

	/**
	 * select Msg RegDate List
	 * 
	 * @param msgCode
	 * @param number
	 * @param group
	 * @param firstdate
	 * @param lastdate
	 * @param page
	 * @param row
	 * 
	 * @return
	 */
	public Cursor selectMsgListRegDate (String msgCode, String number, String group, String firstdate, String lastdate, int page, int row) {
		return mDB.selectMsgListRegDate(msgCode, number, group, firstdate, lastdate, page, row);
	}

	/**
	 * select Msg TrdDate List
	 * 
	 * @param msgCode
	 * @param number
	 * @param group
	 * @param firstdate
	 * @param lastdate
	 * @param page
	 * @param row
	 * 
	 * @return
	 */
	public Cursor selectMsgListTrdDate (String msgCode, String number, String group, String firstdate, String lastdate, int page, int row) {
		return mDB.selectMsgListTrdDate(msgCode, number, group, firstdate, lastdate, page, row);
	}

	/**
	 * select MsgGrp RegDate List
	 * 
	 * @param number
	 * @param group
	 * @param firstdate
	 * @param lastdate
	 * @param page
	 * @param row
	 * @param msgCode
	 * @return
	 */
	public Cursor selectMsgGrpListRegDate (String number, String group, String firstdate, String lastdate, int page, int row, String... msgCode) {
		return mDB.selectMsgGrpListRegDate(number, group, firstdate, lastdate, page, row, msgCode);
	}

	/**
	 * select MsgGrp TrdDate List
	 * 
	 * @param number
	 * @param group
	 * @param firstdate
	 * @param lastdate
	 * @param page
	 * @param row
	 * @param msgCode
	 * @return
	 */
	public Cursor selectMsgGrpListTrdDate (String number, String group, String firstdate, String lastdate, int page, int row, String... msgCode) {
		return mDB.selectMsgGrpListTrdDate(number, group, firstdate, lastdate, page, row, msgCode);
	}

	/**
	 * select Msg
	 * 
	 * @param msgId
	 * @return
	 */
	public Msg selectMsgWhereMsgId (String msgId) {
		return mDB.selectMsgWhereMsgId(msgId);
	}

	/**
	 * select Msg
	 * 
	 * @param userMsgID
	 * @return
	 */
	public Msg selectMsgWhereUserMsgId (String userMsgID) {
		return mDB.selectMsgWhereUserMsgId(userMsgID);
	}

	/**
	 * update MsgGrp
	 * 
	 * @param msgCode
	 * @param values
	 * @return
	 */
	public long updateMsgGrp (String msgCode, ContentValues values) {
		return mDB.updateMsgGrp(msgCode, values);
	}

	/**
	 * update read msg
	 * 
	 * @param msgGrpCd
	 * @param firstUserMsgId
	 * @param lastUserMsgId
	 * @return
	 */
	public long updateReadMsg (String msgGrpCd, String firstUserMsgId, String lastUserMsgId) {
		return mDB.updateReadMsg(msgGrpCd, firstUserMsgId, lastUserMsgId);
	}

	/**
	 * update read msg where userMsgId
	 * 
	 * @param userMsgId
	 * @return
	 */
	public long updateReadMsgWhereUserMsgId (String userMsgId) {
		return mDB.updateReadMsgWhereUserMsgId(userMsgId);
	}

	/**
	 * update read msg where msgId
	 * 
	 * @param msgId
	 * @return
	 */
	public long updateReadMsgWhereMsgId (String msgId) {
		return mDB.updateReadMsgWhereMsgId(msgId);
	}

	/**
	 * delete Msg
	 * 
	 * @param userMsgId
	 * @return
	 */
	public long deleteUserMsgId (String userMsgId) {
		return mDB.deleteUserMsgId(userMsgId);
	}

	/**
	 * delete Msg
	 * 
	 * @param MsgId
	 * @return
	 */
	public long deleteMsgId (String MsgId) {
		return mDB.deleteMsgId(MsgId);
	}

	/**
	 * delete MsgGrp
	 * 
	 * @param msgCode
	 * @return
	 */
	public long deleteMsgGrp (String msgCode) {
		return mDB.deleteMsgGrp(msgCode);
	}

	/**
	 * delete expire Msg
	 * 
	 * @return
	 */
	public long deleteExpireMsg () {
		return mDB.deleteExpireMsg();
	}

	/**
	 * delete empty MsgGrp
	 * 
	 * @return
	 */
	public void deleteEmptyMsgGrp () {
		mDB.deleteEmptyMsgGrp();
	}

	/**
	 * delete all
	 */
	public void deleteAll () {
		mDB.deleteAll();
	}

	/*
	 * ===================================================== [end] database =====================================================
	 */

	public void setNetworkTimeout(int timeout){
		PMSUtil.setNetworkTimeout(mContext, timeout);
	}
	public void setNetworkRetryCount(int count){
		PMSUtil.setNetworkRetryCount(mContext, count);
	}
	public void setNetworkBackoffMultiplier(float backoffMultiplier){
		PMSUtil.setNetworkBackoffMultiplier(mContext, backoffMultiplier);
	}

	public void setNotificationClickActivityClass(String action, Class activity) {
		PMSUtil.setActivityToMoveWhenClick(mContext, action, activity, false);
	}

	public void setNotificationClickActivityUseBackStack(String action, Class activity) {
		PMSUtil.setActivityToMoveWhenClick(mContext, action, activity, true);
	}

	public void setNotificationClickActivityFlag(int flags) {
		PMSUtil.setNotificationClickActivityFlag(mContext, flags);
	}

	public void setNotificationClickActivityFlagEnable(boolean value) {
		PMSUtil.setNotificationClickActivityFlagEnable(mContext, value);
	}

	public void createNotificationChannel(){
		PMSUtil.createNotificationChannel(mContext);
	}
}
