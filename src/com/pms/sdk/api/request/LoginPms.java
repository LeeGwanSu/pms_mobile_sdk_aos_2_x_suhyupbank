package com.pms.sdk.api.request;

import android.content.Context;

import com.pms.sdk.api.APIException;
import com.pms.sdk.api.APIManager.APICallback;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.DataKeyUtil;
import com.pms.sdk.common.util.PMSUtil;
import com.pms.sdk.common.util.StringUtil;
import org.json.JSONException;
import org.json.JSONObject;

public class LoginPms extends BaseRequest {

    public LoginPms(Context context) {
        super(context);
    }

    /**
     * get param
     *
     * @return
     */
    public JSONObject getParam(String custId, String custIdCom, JSONObject userData) {
        JSONObject jobj;

        try {
            jobj = new JSONObject();
            jobj.put("appKey", PMSUtil.getApplicationKey(mContext));
            jobj.put("uuid", PMSUtil.getUUID(mContext));
            jobj.put("custId", custId);
            jobj.put("custIdCom", custIdCom);

            if (userData != null) {
                jobj.put("userData", userData);
            }

            return jobj;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * request
     *
     * @param custId
     * @param userData
     * @param apiCallback
     */
    public void request(String custId, String custIdCom, final JSONObject userData, final APICallback apiCallback) {
        try {
            DataKeyUtil.setDBKey(mContext, DB_CUST_ID, custId);
            if (!custId.equals(DataKeyUtil.getDBKey(mContext, DB_LOGINED_CUST_ID))) {
                // 기존의 custId와 auth할려는 custId가 다르다면, DB초기화
                CLog.i("LoginPms:new user");
                mDB.deleteAll();
                DataKeyUtil.setDBKey(mContext, DB_MAX_USER_MSG_ID, "-1");
            }

            if (!custIdCom.equals(DataKeyUtil.getDBKey(mContext, DB_CUST_ID_COM))) {
                DataKeyUtil.setDBKey(mContext, DB_CUST_ID_COM, custIdCom);
            }

            apiManager.call(API_LOGIN_PMS, getParam(custId, custIdCom, userData), new APICallback() {
                @Override
                public void response(String code, JSONObject json) {
                    if (CODE_SUCCESS.equals(code)) {
                        requiredResultProc(json);
                    }
                    if (apiCallback != null) {
                        apiCallback.response(code, json);
                    }
                }
            });
        } catch (Exception e) {
            JSONObject errResult = new JSONObject();
            String code = e instanceof APIException ? ((APIException) e).getCode() : CODE_UNKNOWN_ERROR;
            String msg = e instanceof APIException ? ((APIException) e).getMsg() : e.getMessage();
            try {
                errResult.put(KEY_API_CODE, code);
                errResult.put(KEY_API_MSG, msg);
            } catch (JSONException jsonException) {
                jsonException.printStackTrace();
            }
            apiCallback.response(CODE_UNKNOWN_ERROR, errResult);
        }
    }

    /**
     * required result proccess
     *
     * @param json
     */
    private boolean requiredResultProc(JSONObject json) {
        String custId = PMSUtil.getCustId(mContext);
        if (!StringUtil.isEmpty(custId)) {
            DataKeyUtil.setDBKey(mContext, DB_LOGINED_CUST_ID, PMSUtil.getCustId(mContext));
        }

        try {
            DataKeyUtil.setDBKey(mContext, DB_DUP_FLAG, json.getString("dupflag"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return true;
    }
}
