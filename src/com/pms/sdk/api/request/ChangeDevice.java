package com.pms.sdk.api.request;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

import com.pms.sdk.api.APIException;
import com.pms.sdk.api.APIManager.APICallback;
import com.pms.sdk.common.util.PMSUtil;

public class ChangeDevice extends BaseRequest {

	public ChangeDevice(Context context) {
		super(context);
	}

	/**
	 * get param
	 * 
	 * @return
	 */
	public JSONObject getParam (String cFlag) {
		JSONObject jobj;

		try {
			jobj = new JSONObject();

			// new version
			jobj.put("cFlag", cFlag);
			jobj.put("custId", PMSUtil.getCustId(mContext));

			return jobj;
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * request
	 * 
	 * @param apiCallback
	 */
	public void request (String CFlag, final APICallback apiCallback) {
		try {
			apiManager.call(API_CHANGEDEVICE_PMS, getParam(CFlag), new APICallback() {
				@Override
				public void response (String code, JSONObject json) {
					if (CODE_SUCCESS.equals(code)) {
						requiredResultProc(json);
					}
					if (apiCallback != null) {
						apiCallback.response(code, json);
					}
				}
			});
		} catch (Exception e) {
			JSONObject errResult = new JSONObject();
			String code = e instanceof APIException ? ((APIException) e).getCode() : CODE_UNKNOWN_ERROR;
			String msg = e instanceof APIException ? ((APIException) e).getMsg() : e.getMessage();
			try {
				errResult.put(KEY_API_CODE, code);
				errResult.put(KEY_API_MSG, msg);
			} catch (JSONException jsonException) {
				jsonException.printStackTrace();
			}
			apiCallback.response(CODE_UNKNOWN_ERROR, errResult);
		}
	}

	/**
	 * required result proccess
	 * 
	 * @param json
	 */
	private boolean requiredResultProc (JSONObject json) {
		return true;
	}
}
