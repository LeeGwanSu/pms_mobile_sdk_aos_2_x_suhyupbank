package com.pms.sdk.api.request;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

import com.pms.sdk.api.APIException;
import com.pms.sdk.api.APIManager.APICallback;

public class ClickMsg extends BaseRequest {

	public ClickMsg(Context context) {
		super(context);
	}

	/**
	 * get param
	 * 
	 * @return
	 */
	public JSONObject getParam (JSONArray clicks) {
		JSONObject jobj;

		try {
			jobj = new JSONObject();
			jobj.put("clicks", clicks);

			return jobj;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * 
	 * @param clicks
	 * @param apiCallback
	 */
	public void request (JSONArray clicks, final APICallback apiCallback) {
		try {
			apiManager.call(API_CLICK_MSG, getParam(clicks), new APICallback() {
				@Override
				public void response (String code, JSONObject json) {
					if (CODE_SUCCESS.equals(code)) {
						requiredResultProc(json);
					}
					if (apiCallback != null) {
						apiCallback.response(code, json);
					}
				}
			});
		} catch (Exception e) {
			JSONObject errResult = new JSONObject();
			String code = e instanceof APIException ? ((APIException) e).getCode() : CODE_UNKNOWN_ERROR;
			String msg = e instanceof APIException ? ((APIException) e).getMsg() : e.getMessage();
			try {
				errResult.put(KEY_API_CODE, code);
				errResult.put(KEY_API_MSG, msg);
			} catch (JSONException jsonException) {
				jsonException.printStackTrace();
			}
			apiCallback.response(CODE_UNKNOWN_ERROR, errResult);
		}
	}

	/**
	 * required result proccess
	 * 
	 * @param json
	 */
	private boolean requiredResultProc (JSONObject json) {
		return true;
	}
}
