package com.pms.sdk.api.request;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.text.TextUtils;

import com.pms.sdk.api.APIException;
import com.pms.sdk.api.APIManager;
import com.pms.sdk.api.APIManager.APICallback;
import com.pms.sdk.common.util.*;
import com.pms.sdk.push.FCMRequestToken;
import com.pms.sdk.push.mqtt.MQTTService;
import com.pms.sdk.push.mqtt.RestartReceiver;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URI;

public class DeviceCert extends BaseRequest {

    public DeviceCert(Context context) {
        super(context);
    }

    /**
     * get param
     *
     * @return
     */
    public JSONObject getParam(JSONObject userData) {
        JSONObject jobj;

        try {
            jobj = new JSONObject();
            jobj.put("appKey", PMSUtil.getApplicationKey(mContext));
            jobj.put("uuid", PMSUtil.getUUID(mContext));
            jobj.put("pushToken", PMSUtil.getGCMToken(mContext));
            jobj.put("custId", PMSUtil.getCustId(mContext));
            jobj.put("appVer", PhoneState.getAppVersion(mContext));
            jobj.put("sdkVer", PMS_VERSION);
            jobj.put("os", "A");
            jobj.put("osVer", PhoneState.getOsVersion());
            jobj.put("device", PhoneState.getDeviceName());
            jobj.put("sessCnt", "1");

            if (userData != null) {
                jobj.put("userData", userData);
            }

            return jobj;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * request
     *
     * @param apiCallback
     */
    public void request(final JSONObject userData, final APICallback apiCallback) {
        String custId = PMSUtil.getCustId(mContext);

        final boolean isCheckUUID = PhoneState.createDeviceToken(mContext);

        if (!custId.equals(DataKeyUtil.getDBKey(mContext, DB_LOGINED_CUST_ID))) {
            // 기존의 custId와 auth할려는 custId가 다르다면, DB초기화
            CLog.i("DeviceCert:new user");
            mDB.deleteAll();
            DataKeyUtil.setDBKey(mContext, DB_MAX_USER_MSG_ID, "-1");
        }

        CLog.i("DeviceCert:validate ok");

		if(PMSUtil.hasFirebaseInstanceIdApi()) {
			new FCMRequestToken(mContext,
			                    ProPertiesFileUtil.getString(mContext, PRO_GCM_PROJECT_ID),
			                    new FCMRequestToken.Callback() {
				                    @Override
				                    public void callback(boolean isSuccess, String message) {
					                    if (StringUtil.isEmpty(
							                    PMSUtil.getGCMToken(mContext)) || NO_TOKEN.equals(
							                    PMSUtil.getGCMToken(mContext))) {
						                    // check push token
						                    CLog.i("DeviceCert:no push token");
						                    DataKeyUtil.setDBKey(mContext, DB_GCM_TOKEN, NO_TOKEN);
					                    }

					                    try {
						                    apiManager.call(API_DEVICE_CERT, getParam(userData),
						                                    new APICallback() {
							                                    @Override
							                                    public void response(String code,
							                                                         JSONObject json) {
								                                    if (CODE_SUCCESS.equals(code)) {
									                                    PMSUtil.setDeviceCertStatus(
											                                    mContext,
											                                    DEVICECERT_COMPLETE);
									                                    requiredResultProc(json);
								                                    }

								                                    if (apiCallback != null) {
									                                    apiCallback.response(code,
									                                                         json);
								                                    }
							                                    }
						                                    });
					                    } catch (Exception e) {
						                    JSONObject errResult = new JSONObject();
						                    String code = e instanceof APIException ? ((APIException) e).getCode() : CODE_UNKNOWN_ERROR;
						                    String msg = e instanceof APIException ? ((APIException) e).getMsg() : e.getMessage();
						                    try {
							                    errResult.put(KEY_API_CODE, code);
							                    errResult.put(KEY_API_MSG, msg);
						                    } catch (JSONException jsonException) {
							                    jsonException.printStackTrace();
						                    }
						                    apiCallback.response(CODE_UNKNOWN_ERROR, errResult);
					                    }
				                    }
			                    }).execute();
		} else {
			try {
				apiManager.call(API_DEVICE_CERT, getParam(userData),
				                new APICallback() {
					                @Override
					                public void response(String code,
					                                     JSONObject json) {
						                if (CODE_SUCCESS.equals(code)) {
							                PMSUtil.setDeviceCertStatus(
									                mContext,
									                DEVICECERT_COMPLETE);
							                requiredResultProc(json);
						                }

						                if (apiCallback != null) {
							                apiCallback.response(code,
							                                     json);
						                }
					                }
				                });
			} catch (Exception e) {
				JSONObject errResult = new JSONObject();
				String code = e instanceof APIException ? ((APIException) e).getCode() : CODE_UNKNOWN_ERROR;
				String msg = e instanceof APIException ? ((APIException) e).getMsg() : e.getMessage();
				try {
					errResult.put(KEY_API_CODE, code);
					errResult.put(KEY_API_MSG, msg);
				} catch (JSONException jsonException) {
					jsonException.printStackTrace();
				}
				apiCallback.response(CODE_UNKNOWN_ERROR, errResult);
			}
		}
    }

    /**
     * required result proccess
     *
     * @param json
     */
    @SuppressLint("DefaultLocale")
    private boolean requiredResultProc(JSONObject json) {
        try {
            PMSUtil.setAppUserId(mContext, json.getString("appUserId"));
            PMSUtil.setEncKey(mContext, json.getString("encKey"));

            if (json.has("license")) {
                PMSUtil.setLicenseFlag(mContext, json.getString("license"));
                if (LICENSE_EXPIRED.equals(PMSUtil.getLicenseFlag(mContext)) || LICENSE_PENDING.equals(PMSUtil.getLicenseFlag(mContext))) {
                    mContext.stopService(new Intent(mContext, MQTTService.class));
                }
            }

            if (json.has("sdkHalt")) {
                PMSUtil.setSDKLockFlag(mContext, json.getString("sdkHalt"));
                if (FLAG_Y.equals(PMSUtil.getSDKLockFlag(mContext))) {
                    mContext.stopService(new Intent(mContext, MQTTService.class));
                }
            }

            // set msg flag
            DataKeyUtil.setDBKey(mContext, DB_API_LOG_FLAG, json.getString("collectApiLogFlag"));
            DataKeyUtil.setDBKey(mContext, DB_PRIVATE_LOG_FLAG, json.getString("collectPrivateLogFlag"));
            String custId = PMSUtil.getCustId(mContext);
            if (!StringUtil.isEmpty(custId)) {
                DataKeyUtil.setDBKey(mContext, DB_LOGINED_CUST_ID, PMSUtil.getCustId(mContext));
            }

            // set config flag
            if (StringUtil.isEmpty(DataKeyUtil.getDBKey(mContext, DB_NOTI_FLAG))) {
                DataKeyUtil.setDBKey(mContext, DB_NOTI_FLAG, json.getString("notiFlag"));
            }
            if (StringUtil.isEmpty(DataKeyUtil.getDBKey(mContext, DB_MSG_FLAG))) {
                DataKeyUtil.setDBKey(mContext, DB_MSG_FLAG, json.getString("msgFlag"));
            }
            if (StringUtil.isEmpty(DataKeyUtil.getDBKey(mContext, DB_MKT_FLAG))) {
                DataKeyUtil.setDBKey(mContext, DB_MKT_FLAG, json.getString("mktFlag"));
            }
            if ((DataKeyUtil.getDBKey(mContext, DB_MSG_FLAG).equals(json.getString("msgFlag")) == false)
                    || (DataKeyUtil.getDBKey(mContext, DB_NOTI_FLAG).equals(json.getString("notiFlag")) == false)
                    || (DataKeyUtil.getDBKey(mContext, DB_MKT_FLAG).equals(json.getString("mktFlag")) == false)) {
                new SetConfig(mContext).request(DataKeyUtil.getDBKey(mContext, DB_MSG_FLAG), DataKeyUtil.getDBKey(mContext, DB_NOTI_FLAG),
                        DataKeyUtil.getDBKey(mContext, DB_MKT_FLAG), null);
            }

            // readMsg
            final JSONArray readArray = PMSUtil.arrayFromPrefs(mContext, PREF_READ_LIST);
            if (readArray.length() > 0) {
                // call readMsg
                new ReadMsg(mContext).request(readArray, new APICallback() {
                    @Override
                    public void response(String code, JSONObject json) {
                        if (CODE_SUCCESS.equals(code)) {
                            // delete readMsg
                            new Prefs(mContext).putString(PREF_READ_LIST, "");
                        }

                        if (CODE_PARSING_JSON_ERROR.equals(code)) {
                            for (int i = 0; i < readArray.length(); i++) {
                                try {
                                    if ((readArray.get(i) instanceof JSONObject) == false) {
                                        readArray.put(i, PMSUtil.getReadParam(readArray.getString(i)));
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                            new ReadMsg(mContext).request(readArray, new APICallback() {
                                @Override
                                public void response(String code, JSONObject json) {
                                    if (CODE_SUCCESS.equals(code)) {
                                        new Prefs(mContext).putString(PREF_READ_LIST, "");
                                    }
                                }
                            });
                        }
                    }
                });
            } else {
                CLog.i("readArray is null");
            }

            // clickMsg
            JSONArray clickArray = PMSUtil.arrayFromPrefs(mContext, PREF_CLICK_LIST);
            if (clickArray.length() > 0) {
                // call clickMsg
                new ClickMsg(mContext).request(clickArray, new APICallback() {
                    @Override
                    public void response(String code, JSONObject json) {
                        if (CODE_SUCCESS.equals(code)) {
                            // delete clickMsg
                            new Prefs(mContext).putString(PREF_CLICK_LIST, "");
                        }
                    }
                });
            } else {
                CLog.i("clickArray is null");
            }

            // mqttFlag Y/N
            if (FLAG_Y.equals(PMSUtil.getMQTTFlag(mContext))) {
                String flag = json.getString("privateFlag");
                DataKeyUtil.setDBKey(mContext, DB_PRIVATE_FLAG, flag);
                if (FLAG_Y.equals(flag)) {
                    String protocol = json.getString("privateProtocol");
                    String protocolTemp = "";
                    try {
                        URI uri = new URI(PMSUtil.getMQTTServerUrl(mContext));
                        if (protocol.equals(PROTOCOL_TCP)) {
                            protocolTemp = protocol.toLowerCase() + "cp";
                        } else if (protocol.equals(PROTOCOL_SSL)) {
                            protocolTemp = protocol.toLowerCase() + "sl";
                        }

                        if (uri.getScheme().equals(protocolTemp)) {
                            DataKeyUtil.setDBKey(mContext, DB_PRIVATE_PROTOCOL, protocol);
                        } else {
                            DataKeyUtil.setDBKey(mContext, DB_PRIVATE_PROTOCOL, protocol);
                            mContext.stopService(new Intent(mContext, MQTTService.class));
                        }
                    } catch (NullPointerException e) {
                        DataKeyUtil.setDBKey(mContext, DB_PRIVATE_PROTOCOL, protocol);
                    }

                    Intent i = new Intent(mContext, RestartReceiver.class);
                    i.setAction(MQTTService.ACTION_START);
                    mContext.sendBroadcast(i);
                } else {
                    mContext.stopService(new Intent(mContext, MQTTService.class));
                }
            }

            // LogFlag Y/N
            if ((FLAG_N.equals(DataKeyUtil.getDBKey(mContext, DB_API_LOG_FLAG)) && FLAG_N.equals(DataKeyUtil.getDBKey(mContext, DB_PRIVATE_LOG_FLAG))) == false) {
                setCollectLog();
            }

            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private void setCollectLog() {
        boolean isAfter = false;
        String beforeDate = DataKeyUtil.getDBKey(mContext, DB_YESTERDAY);
        String today = DateUtil.getNowDateMo();

        try {
            isAfter = DateUtil.isDateAfter(beforeDate, today);
        } catch (Exception e) {
            isAfter = false;
        }

        if (isAfter) {
            DataKeyUtil.setDBKey(mContext, DB_ONEDAY_LOG, "N");
        }

        if (FLAG_N.equals(DataKeyUtil.getDBKey(mContext, DB_ONEDAY_LOG)) == false) {
            if (beforeDate.equals("")) {
                beforeDate = DateUtil.getNowDateMo();
                DataKeyUtil.setDBKey(mContext, DB_YESTERDAY, beforeDate);
            }
            new CollectLog(mContext).request(beforeDate, null);
        }
    }
}
