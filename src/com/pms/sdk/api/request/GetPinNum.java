package com.pms.sdk.api.request;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

import com.pms.sdk.api.APIException;
import com.pms.sdk.api.APIManager.APICallback;

public class GetPinNum extends BaseRequest {

	public GetPinNum(Context context) {
		super(context);
	}

	/**
	 * request
	 * 
	 * @param apiCallback
	 */
	public void request (final APICallback apiCallback) {
		try {
			apiManager.call(API_GET_PIN_NUM, new JSONObject(), new APICallback() {
				@Override
				public void response (String code, JSONObject json) {
					if (CODE_SUCCESS.equals(code)) {
						requiredResultProc(json);
					}
					if (apiCallback != null) {
						apiCallback.response(code, json);
					}
				}
			});
		} catch (Exception e) {
			JSONObject errResult = new JSONObject();
			String code = e instanceof APIException ? ((APIException) e).getCode() : CODE_UNKNOWN_ERROR;
			String msg = e instanceof APIException ? ((APIException) e).getMsg() : e.getMessage();
			try {
				errResult.put(KEY_API_CODE, code);
				errResult.put(KEY_API_MSG, msg);
			} catch (JSONException jsonException) {
				jsonException.printStackTrace();
			}
			apiCallback.response(CODE_UNKNOWN_ERROR, errResult);
		}
	}

	/**
	 * required result proccess
	 * 
	 * @param json
	 */
	private boolean requiredResultProc (JSONObject json) {
		return true;
	}
}
