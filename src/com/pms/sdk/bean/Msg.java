package com.pms.sdk.bean;

import org.json.JSONObject;

import android.database.Cursor;

/**
 * @since 2012.12.26
 * @author erzisk
 * @description message bean
 */
public class Msg {

	public static final String TABLE_NAME = "TBL_MSG";
	public static final String _ID = "_id";
	public static final String USER_MSG_ID = "USER_MSG_ID";
	public static final String MSG_GRP_NM = "MSG_GRP_NM";
	public static final String APP_LINK = "APP_LINK";
	public static final String ICON_NAME = "ICON_NAME";
	public static final String MSG_ID = "MSG_ID";
	public static final String PUSH_TITLE = "PUSH_TITLE";
	public static final String PUSH_MSG = "PUSH_MSG";
	public static final String PUSH_IMG = "PUSH_IMG";
	public static final String IN_APP_IMG = "IN_APP_IMG";
	public static final String MSG_TEXT = "MSG_TEXT";
	public static final String MAP1 = "MAP1";
	public static final String MAP2 = "MAP2";
	public static final String MAP3 = "MAP3";
	public static final String MAP4 = "MAP4";
	public static final String MAP5 = "MAP5";
	public static final String MAP6 = "MAP6";
	public static final String MAP7 = "MAP7";
	public static final String MSG_TYPE = "MSG_TYPE";
	public static final String READ_YN = "READ_YN";
	public static final String DEL_YN = "DEL_YN";
	public static final String EXPIRE_DATE = "EXPIRE_DATE";
	public static final String REG_DATE = "REG_DATE";
	public static final String TRD_TIME = "TRD_TIME";
	public static final String MSG_GRP_CD = "MSG_GRP_CD";

	// [code flag]
	// default
	public static final String CODE_DEFAULT = "00000";

	// [type flag]
	// text
	public static final String TYPE_T = "T";
	// attach
	public static final String TYPE_A = "A";
	// html
	public static final String TYPE_H = "H";
	// link
	public static final String TYPE_L = "L";

	// [read falg]
	// read
	public static final String READ_Y = "Y";
	// unread
	public static final String READ_N = "N";

	public static final int ROW_COUNT = 50;

	public static final String CREATE_MSG = "CREATE TABLE " + TABLE_NAME + "( " + _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + USER_MSG_ID
			+ " INTEGER, " + MSG_GRP_NM + " TEXT, " + APP_LINK + " TEXT, " + ICON_NAME + " TEXT, " + MSG_ID + " INTEGER, " + PUSH_TITLE + " TEXT, "
			+ PUSH_MSG + " TEXT, " + PUSH_IMG + " TEXT, " + IN_APP_IMG + " TEXT, " + MSG_TEXT + " TEXT, " + MAP1 + " TEXT, " + MAP2 + " TEXT, "
			+ MAP3 + " TEXT, " + MAP4 + " TEXT, " + MAP5 + " TEXT, " + MAP6 + " TEXT, " + MAP7 + " TEXT, " + MSG_TYPE + " TEXT, " + READ_YN
			+ " TEXT, " + DEL_YN + " TEXT, " + EXPIRE_DATE + " TEXT, " + REG_DATE + " TEXT, " + TRD_TIME + " TEXT, " + MSG_GRP_CD + " TEXT " + ");";

	public String id = "-1";
	public String userMsgId = "";
	public String msgGrpNm = "";
	public String appLink = "";
	public String iconName = "";
	public String msgId = "";
	public String pushTitle = "";
	public String pushMsg = "";
	public String pushImg = "";
	public String inAppImg = "";
	public String msgText = "";
	public String map1 = "";
	public String map2 = "";
	public String map3 = "";
	public String map4 = "";
	public long map5 = 0;
	public String map6 = "";
	public String map7 = "";
	public String msgType = "";
	public String readYn = "";
	public String delYn = "";
	public String expireDate = "";
	public String regDate = "";
	public String trdTime = "";
	public String msgGrpCd = "";

	public Msg() {
	}

	public Msg(Cursor c) {
		// Convert.cursorToBean(c, this);

		id = c.getString(c.getColumnIndexOrThrow(_ID));
		userMsgId = c.getString(c.getColumnIndexOrThrow(USER_MSG_ID));
		msgGrpNm = c.getString(c.getColumnIndexOrThrow(MSG_GRP_NM));
		appLink = c.getString(c.getColumnIndexOrThrow(APP_LINK));
		iconName = c.getString(c.getColumnIndexOrThrow(ICON_NAME));
		msgId = c.getString(c.getColumnIndexOrThrow(MSG_ID));
		pushTitle = c.getString(c.getColumnIndexOrThrow(PUSH_TITLE));
		pushMsg = c.getString(c.getColumnIndexOrThrow(PUSH_MSG));
		pushImg = c.getString(c.getColumnIndexOrThrow(PUSH_IMG));
		inAppImg = c.getString(c.getColumnIndexOrThrow(IN_APP_IMG));
		msgText = c.getString(c.getColumnIndexOrThrow(MSG_TEXT));
		msgType = c.getString(c.getColumnIndexOrThrow(MSG_TYPE));
		map1 = c.getString(c.getColumnIndexOrThrow(MAP1));
		map2 = c.getString(c.getColumnIndexOrThrow(MAP2));
		map3 = c.getString(c.getColumnIndexOrThrow(MAP3));
		map4 = c.getString(c.getColumnIndexOrThrow(MAP4));
		map5 = c.getLong(c.getColumnIndexOrThrow(MAP5));
		map6 = c.getString(c.getColumnIndexOrThrow(MAP6));
		map7 = c.getString(c.getColumnIndexOrThrow(MAP7));
		readYn = c.getString(c.getColumnIndexOrThrow(READ_YN));
		delYn = c.getString(c.getColumnIndexOrThrow(DEL_YN));
		expireDate = c.getString(c.getColumnIndexOrThrow(EXPIRE_DATE));
		regDate = c.getString(c.getColumnIndexOrThrow(REG_DATE));
		trdTime = c.getString(c.getColumnIndexOrThrow(TRD_TIME));
		msgGrpCd = c.getString(c.getColumnIndexOrThrow(MSG_GRP_CD));
	}

	public Msg(JSONObject jo) {
		try {
			if (jo.has("userMsgId")) {
				userMsgId = jo.getString("userMsgId");
			}
			if (jo.has("msgGrpNm")) {
				msgGrpNm = jo.getString("msgGrpNm");
			}
			if (jo.has("appLink")) {
				appLink = jo.getString("appLink");
			}
			if (jo.has("iconName")) {
				iconName = jo.getString("iconName");
			}
			if (jo.has("msgId")) {
				msgId = jo.getString("msgId");
			}
			if (jo.has("pushTitle")) {
				pushTitle = jo.getString("pushTitle");
			}
			if (jo.has("pushMsg")) {
				pushMsg = jo.getString("pushMsg");
			}
			if (jo.has("pushImg")) {
				pushImg = jo.getString("pushImg");
			}
			if (jo.has("inAppImg")) {
				inAppImg = jo.getString("inAppImg");
			}
			if (jo.has("msgText")) {
				msgText = jo.getString("msgText");
			}
			if (jo.has("msgType")) {
				msgType = jo.getString("msgType");
			}
			if (jo.has("map1")) {
				map1 = jo.getString("map1");
			}
			if (jo.has("map2")) {
				map2 = jo.getString("map2");
			}
			if (jo.has("map3")) {
				map3 = jo.getString("map3");
			}
			if (jo.has("map4")) {
				map4 = jo.getString("map4");
			}
			if (jo.has("map5")) {
				map5 = jo.getLong("map5");
			}
			if (jo.has("map6")) {
				map6 = jo.getString("map6");
			}
			if (jo.has("map7")) {
				map7 = jo.getString("map7");
			}
			if (jo.has("readYn")) {
				readYn = jo.getString("readYn");
			}
			if (jo.has("expireDate")) {
				expireDate = jo.getString("expireDate");
			}
			if (jo.has("regDate")) {
				regDate = jo.getString("regDate");
			}
			if (jo.has("trdTime")) {
				trdTime = jo.getString("trdTime");
			}
			if (jo.has("msgGrpCd")) {
				msgGrpCd = jo.getString("msgGrpCd");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}