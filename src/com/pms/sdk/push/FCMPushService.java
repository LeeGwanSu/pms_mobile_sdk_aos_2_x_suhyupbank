package com.pms.sdk.push;

import android.content.Intent;

import com.pms.sdk.IPMSConsts;
import com.pms.sdk.common.util.PMSUtil;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.DataKeyUtil;
import com.pms.sdk.common.util.ProPertiesFileUtil;
import com.pms.sdk.push.mqtt.MQTTService;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONObject;

/**
 * Created by ydmner on 7/27/16.
 */
public class FCMPushService extends FirebaseMessagingService implements IPMSConsts {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        try {
            CLog.i("FCM OnMessageReceived From : " + remoteMessage.getFrom());
            CLog.i("Message data payload: " + remoteMessage.getData());

//            if (PMSUtil.getGCMProjectId(this).equals(remoteMessage.getFrom())) {
//                Intent broadcastIntent = new Intent(getApplicationContext(), PushReceiver.class);
//                broadcastIntent.setAction(ACTION_RECEIVE);
////                Intent broadcastIntent = new Intent(ACTION_RECEIVE);
//                broadcastIntent.addCategory(getApplication().getPackageName());
//
//                broadcastIntent.putExtra(MQTTService.KEY_MSG, new JSONObject(remoteMessage.getData()).toString());
//                broadcastIntent.putExtra("message_id", remoteMessage.getMessageId());
//                sendBroadcast(broadcastIntent);
//            } else {
//                CLog.e("No Match SenderID");
//            }
            //2019.01.16 수협은행 요청으로 다중 프로젝트 FCM 용으로 만듬
            Intent broadcastIntent = new Intent(getApplicationContext(), PushReceiver.class);
            broadcastIntent.setAction(ACTION_RECEIVE);
//                Intent broadcastIntent = new Intent(ACTION_RECEIVE);
            broadcastIntent.addCategory(getApplication().getPackageName());
            broadcastIntent.putExtra(MQTTService.KEY_MSG, new JSONObject(remoteMessage.getData()).toString());
            broadcastIntent.putExtra("message_id", remoteMessage.getMessageId());
            sendBroadcast(broadcastIntent);

            Intent intentPush = null;
            String receiverClass = null;
            receiverClass = ProPertiesFileUtil.getString(getApplicationContext(), PRO_PUSH_RECEIVER_CLASS);
            if (receiverClass != null) {
                try {
                    Class<?> cls = Class.forName(receiverClass);
                    intentPush = new Intent(getApplicationContext(), cls);
                    intentPush.putExtras(remoteMessage.toIntent());
                    intentPush.setAction(RECEIVER_PUSH);
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
            if (intentPush == null) {
                intentPush = new Intent(RECEIVER_PUSH).putExtras(remoteMessage.toIntent());
            }

            if (intentPush != null) {
                intentPush.addCategory(getApplicationContext().getPackageName());
                getApplicationContext().sendBroadcast(intentPush);
            }

        } catch (Exception e) {
            CLog.e(e.getMessage());
        }
    }

    @Override
    public void onMessageSent(String msgId) {
        CLog.e("onMessageSent() " + msgId);
        super.onMessageSent(msgId);
    }

    @Override
    public void onSendError(String s, Exception e) {
        CLog.e("onSendError() " + s + ", " + e.toString());
        super.onSendError(s, e);
    }

    @Override
    public void onNewToken(String s)
    {
        super.onNewToken(s);
        DataKeyUtil.setDBKey(getApplicationContext(), DB_GCM_TOKEN, s);
        CLog.i("onNewToken, registration ID = " + s);
    }
}
