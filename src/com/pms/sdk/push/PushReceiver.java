package com.pms.sdk.push;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.text.TextUtils;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.ImageLoader.ImageContainer;
import com.android.volley.toolbox.ImageLoader.ImageListener;
import com.pms.sdk.IPMSConsts;
import com.pms.sdk.PMS;
import com.pms.sdk.api.QueueManager;
import com.pms.sdk.bean.Msg;
import com.pms.sdk.bean.PushMsg;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.DataKeyUtil;
import com.pms.sdk.common.util.PMSUtil;
import com.pms.sdk.common.util.PhoneState;
import com.pms.sdk.common.util.ProPertiesFileUtil;
import com.pms.sdk.common.util.StringUtil;
import com.pms.sdk.db.PMSDB;
import com.pms.sdk.push.mqtt.MQTTService;
import com.pms.sdk.push.mqtt.RestartReceiver;
import com.pms.sdk.view.BitmapLruCache;

import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.List;

/**
 * push receiver
 * 
 * @author erzisk
 * @since 2013.06.07
 */
public class PushReceiver extends BroadcastReceiver implements IPMSConsts {

	private static final int DEFAULT_SHOWING_TIME = 30000;

	private static final int START_TASK_TO_FRONT = 2;

	private static final String USE = "Y";

	public final static int NOTIFICATION_ID = 0x253470;

	private static final int NOTIFICATION_GROUP_SUMMARY_ID = 1;

	private static final String NOTIFICATION_GROUP = "com.apms.sdk.notification_type";

	private static int sNotificationId = NOTIFICATION_GROUP_SUMMARY_ID + 1;

	private final Handler mFinishHandler = new Handler();

	private PowerManager pm;
	private PowerManager.WakeLock wl;

	private boolean mbPushImage = false;

	private Bitmap mPushImage;

	@Override
	public synchronized void onReceive (final Context context, final Intent intent) {
		CLog.i("onReceive() -> " + intent.toString());

		if (!intent.getAction().equals(ACTION_REGISTRATION)) {
			// receive push message
			if (intent.getAction().equals(MQTTService.INTENT_RECEIVED_MSG)) {
				// private server
				CLog.i("onReceive:receive from private server");

			} else if (intent.getAction().equals(ACTION_RECEIVE)) {
				// gcm
				CLog.i("onReceive:receive from GCM");
			}
			String message = intent.getStringExtra(MQTTService.KEY_MSG);

			// set push info
			try {
				JSONObject msgObj = new JSONObject(message);
				if (msgObj.has(KEY_MSG_ID)) {
					intent.putExtra(KEY_MSG_ID, msgObj.getString(KEY_MSG_ID));
				}
				if (msgObj.has(KEY_NOTI_TITLE)) {
					intent.putExtra(KEY_NOTI_TITLE, msgObj.getString(KEY_NOTI_TITLE));
				}
				if (msgObj.has(KEY_MSG_TYPE)) {
					intent.putExtra(KEY_MSG_TYPE, msgObj.getString(KEY_MSG_TYPE));
				}
				if (msgObj.has(KEY_NOTI_MSG)) {
					intent.putExtra(KEY_NOTI_MSG, msgObj.getString(KEY_NOTI_MSG));
				}
				if (msgObj.has(KEY_MSG)) {
					intent.putExtra(KEY_MSG, msgObj.getString(KEY_MSG));
				}
				if (msgObj.has(KEY_SOUND)) {
					intent.putExtra(KEY_SOUND, msgObj.getString(KEY_SOUND));
				}
				if (msgObj.has(KEY_NOTI_IMG)) {
					intent.putExtra(KEY_NOTI_IMG, msgObj.getString(KEY_NOTI_IMG));
				}
				if (msgObj.has(KEY_NOT_POPUP)) {
					intent.putExtra(KEY_NOT_POPUP, msgObj.getString(KEY_NOT_POPUP));
				}
				if (msgObj.has(KEY_DATA)) {
					intent.putExtra(KEY_DATA, msgObj.getString(KEY_DATA));
				}
				onPushMessage(context, intent);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private synchronized void onPushMessage (final Context context, final Intent intent) {
		if (isImagePush(intent.getExtras())) {
			try {
				// image push
				QueueManager queueManager = QueueManager.getInstance();
				RequestQueue queue = queueManager.getRequestQueue();
				ImageLoader imageLoader = new ImageLoader(queue, new BitmapLruCache());
				imageLoader.get(intent.getStringExtra(KEY_NOTI_IMG), new ImageListener() {
					@Override
					public void onResponse (ImageContainer response, boolean isImmediate) {
						if (response == null) {
							CLog.e("response is null");
							return;
						}
						if (response.getBitmap() == null) {
							CLog.e("bitmap is null");
							return;
						}
						mbPushImage = true;
						mPushImage = response.getBitmap();
						CLog.i("imageWidth:" + mPushImage.getWidth());
						onMessage(context, intent);
					}

					@Override
					public void onErrorResponse (VolleyError error) {
						CLog.e("onErrorResponse:" + error.getMessage());
						mbPushImage = false;
						// wrong img url (or exception)
						onMessage(context, intent);
					}
				});
			} catch (Exception e) {
				e.printStackTrace();
				onMessage(context, intent);
			}
		} else {
			// default push
			onMessage(context, intent);
		}
	}

	/**
	 * on message (gcm, private msg receiver)
	 * 
	 * @param context
	 * @param intent
	 */
	@SuppressWarnings({ "deprecation", "unused" })
	private synchronized void onMessage (final Context context, Intent intent) {

		final Bundle extras = intent.getExtras();

		PMS pms = PMS.getInstance(context);

		PushMsg pushMsg = new PushMsg(extras);
		CLog.i(pushMsg + "");

		if (StringUtil.isEmpty(pushMsg.msgId) || StringUtil.isEmpty(pushMsg.notiTitle) || StringUtil.isEmpty(pushMsg.notiMsg)
				|| StringUtil.isEmpty(pushMsg.msgType)) {
			CLog.i("msgId or notiTitle or notiMsg or msgType is null");
			if(PMSUtil.getMQTTFlag(context).equals(FLAG_Y))
			{
				Intent i = new Intent(context, RestartReceiver.class);
				i.setAction(ACTION_FORCE_START);
				context.sendBroadcast(i);
			}
			return;
		}

		PMSDB db = PMSDB.getInstance(context);

		// check already exist msg
		Msg existMsg = db.selectMsgWhereMsgId(pushMsg.msgId);
		if (existMsg != null && existMsg.msgId.equals(pushMsg.msgId)) {
			CLog.i("already exist msg");
			return;
		}

		// insert (temp) new msg
		Msg newMsg = new Msg();
		newMsg.readYn = Msg.READ_N;
		newMsg.delYn = Msg.READ_N;
		newMsg.msgGrpCd = "999999";
		newMsg.expireDate = "0";
		newMsg.msgId = pushMsg.msgId;

		db.insertMsg(newMsg);

		// show noti
		// ** 수정 ** 기존 코드는 PREF_MSG_FLAG로 되어 있어 NOTI_FLAG로 변경 함
		CLog.i("NOTI FLAG : " + DataKeyUtil.getDBKey(context, DB_NOTI_FLAG));

		if (USE.equals(DataKeyUtil.getDBKey(context, DB_NOTI_FLAG)) || StringUtil.isEmpty(DataKeyUtil.getDBKey(context, DB_NOTI_FLAG))) {
			// check push flag

			if (USE.equals(ProPertiesFileUtil.getString(context, PRO_SCREEN_WAKEUP_FLAG))) {
				// screen on
				pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
				wl = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "myapp:tag");
				if (!pm.isScreenOn()) {
					wl.acquire();
					mFinishHandler.postDelayed(finishRunnable, DEFAULT_SHOWING_TIME);
				}
			}
			CLog.i("version code :" + Build.VERSION.SDK_INT);

			showNotification(context, extras);

			CLog.i("ALERT FLAG : " + DataKeyUtil.getDBKey(context, DB_ALERT_FLAG));
			CLog.i("NOT POPUP FLAG : " + pushMsg.notPopup);

			if ((USE.equals(pushMsg.notPopup) == true) && (USE.equals(DataKeyUtil.getDBKey(context, DB_ALERT_FLAG)))) {
				showPopup(context, extras);
			}
		}
	}

	/**
	 * show notification
	 * 
	 * @param context
	 * @param extras
	 */
	private synchronized void showNotification (Context context, Bundle extras) {
		CLog.i("showNotification");
		CLog.i("Push Image State ->" + mbPushImage);
		if (mbPushImage) {
			showNotificationImageStyle(context, extras);
		} else {
			showNotificationTextStyle(context, extras);
		}
	}

	/**
	 * show notification text style
	 * 
	 * @param context
	 * @param extras
	 */
	@SuppressLint("InlinedApi")
	private synchronized void showNotificationTextStyle (final Context context, Bundle extras) {
		CLog.i("showNotificationTextStyle");
		// push info
		PushMsg pushMsg = new PushMsg(extras);

		// notification
		int iconId = PMSUtil.getIconId(context);
		int largeIconId = PMSUtil.getLargeIconId(context);

		NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		NotificationCompat.Builder builder;

		String strNotiChannel = DataKeyUtil.getDBKey(context, DB_NOTI_CHANNEL_ID);
		if (StringUtil.isEmpty(strNotiChannel)) {
			strNotiChannel = "0";
		}

		// Notification channel added
		if (Build.VERSION.SDK_INT > Build.VERSION_CODES.N_MR1 && PMSUtil.getTargetVersion(context) > Build.VERSION_CODES.N_MR1) {
			strNotiChannel = createNotiChannel(context, notificationManager, strNotiChannel);
			builder = new NotificationCompat.Builder(context, strNotiChannel);
			builder.setNumber(0);
		}
		else {
			builder = new NotificationCompat.Builder(context);
			builder.setNumber(PMSDB.getInstance(context).selectNewMsgCnt());
		}

		final int noti_mode = ((AudioManager) context.getSystemService(Context.AUDIO_SERVICE)).getRingerMode();
		// checek ring mode
		if (noti_mode == AudioManager.RINGER_MODE_NORMAL) {
			if (FLAG_Y.equals(DataKeyUtil.getDBKey(context, DB_RING_FLAG)) || StringUtil.isEmpty(DataKeyUtil.getDBKey(context, DB_RING_FLAG))) {
				int notiSound = PMSUtil.getNotiSound(context);
				CLog.d("notiSound : "+notiSound);
				if (notiSound > 0) {
					Uri uri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + context.getPackageName() + "/" + notiSound);
					builder.setSound(uri);
				} else {
					Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
					builder.setSound(uri);
				}
			}
		}
		
//		NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
		builder.setContentIntent(makePendingIntent(context, extras));
		builder.setAutoCancel(true);
		builder.setContentText(pushMsg.notiMsg);
		builder.setContentTitle(pushMsg.notiTitle);
		builder.setTicker(pushMsg.notiMsg);
		// setting lights color
		builder.setLights(Notification.FLAG_SHOW_LIGHTS, 1000, 2000);
		// builder.setPriority(Notification.PRIORITY_MAX);

		// set small icon

		CLog.i("small icon :" + iconId);
		if (iconId > 0) {
			builder.setSmallIcon(iconId);
		}

		// set large icon
		CLog.i("large icon :" + largeIconId);
		if (largeIconId > 0) {
			builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), largeIconId));
		}

		// check vibe mode
		if (FLAG_Y.equals(DataKeyUtil.getDBKey(context, DB_VIBE_FLAG)) || StringUtil.isEmpty(DataKeyUtil.getDBKey(context, DB_VIBE_FLAG))) {
			builder.setDefaults(Notification.DEFAULT_VIBRATE);
		}

		//
		if (FLAG_Y.equals(DataKeyUtil.getDBKey(context, DB_NOTI_BIG_TEXT_STYLE)))
		{
			builder.setStyle(new NotificationCompat.BigTextStyle().setBigContentTitle(pushMsg.notiTitle).bigText(pushMsg.notiMsg));
		}

		int notificationId = getNewNotificationId();
		// show notification
//		NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
		{
			if(FLAG_N.equals(ProPertiesFileUtil.getString(context, PRO_NOTI_GROUP_FLAG)))
			{
				notificationManager.cancelAll();
			}
			notificationManager.notify(notificationId, builder.build());
		}
		else
		{
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N && FLAG_Y.equals(ProPertiesFileUtil.getString(context, PRO_NOTI_GROUP_FLAG)))
			{
				notificationManager.cancelAll();
			}
			else
			{
				notificationManager.notify(notificationId, builder.build());
			}
		}
	}

	/**
	 * show notification image style
	 * 
	 * @param context
	 * @param extras
	 */
	@SuppressLint("NewApi")
	private synchronized void showNotificationImageStyle (final Context context, Bundle extras) {
		CLog.i("showNotificationImageStyle");
		// push info
		PushMsg pushMsg = new PushMsg(extras);

		// notification
		int iconId = PMSUtil.getIconId(context);
		int largeIconId = PMSUtil.getLargeIconId(context);
		String msg = "";

		if (StringUtil.isEmpty(PMSUtil.getBigNotiContextMsg(context))) {
			msg = pushMsg.notiMsg;
		} else {
			msg = PMSUtil.getBigNotiContextMsg(context);
		}

		NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		NotificationCompat.Builder builder;

		String strNotiChannel = DataKeyUtil.getDBKey(context, DB_NOTI_CHANNEL_ID);
		if (StringUtil.isEmpty(strNotiChannel)) {
			strNotiChannel = "0";
		}

		// Notification channel added
		if (Build.VERSION.SDK_INT > Build.VERSION_CODES.N_MR1 && PMSUtil.getTargetVersion(context) > Build.VERSION_CODES.N_MR1) {
			strNotiChannel = createNotiChannel(context, notificationManager, strNotiChannel);
			builder = new NotificationCompat.Builder(context, strNotiChannel);
			builder.setNumber(0);
		}
		else {
			builder = new NotificationCompat.Builder(context);
			builder.setNumber(PMSDB.getInstance(context).selectNewMsgCnt());
		}

		final int noti_mode = ((AudioManager) context.getSystemService(Context.AUDIO_SERVICE)).getRingerMode();
		// checek ring mode
		if (noti_mode == AudioManager.RINGER_MODE_NORMAL) {
			if (FLAG_Y.equals(DataKeyUtil.getDBKey(context, DB_RING_FLAG)) || StringUtil.isEmpty(DataKeyUtil.getDBKey(context, DB_RING_FLAG))) {
				int notiSound = PMSUtil.getNotiSound(context);
				CLog.d("notiSound : "+notiSound);
				if (notiSound > 0) {
					Uri uri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + context.getPackageName() + "/" + notiSound);
					builder.setSound(uri);
				} else {
					Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
					builder.setSound(uri);
				}
			}
		}
		
//		Notification.Builder builder = new Notification.Builder(context);

		builder.setContentIntent(makePendingIntent(context, extras));
		builder.setAutoCancel(true);
		builder.setContentText(msg);
		builder.setContentTitle(pushMsg.notiTitle);
		builder.setTicker(pushMsg.notiMsg);
		// setting lights color
		builder.setLights(0xFF00FF00, 1000, 2000);
		// builder.setPriority(Notification.PRIORITY_MAX);

		// set small icon
		CLog.i("small icon :" + iconId);
		if (iconId > 0) {
			builder.setSmallIcon(iconId);
		}

		// set large icon
		CLog.i("large icon :" + largeIconId);
		if (largeIconId > 0) {
			builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), largeIconId));
		}

		// check vibe mode
		if (FLAG_Y.equals(DataKeyUtil.getDBKey(context, DB_VIBE_FLAG)) || StringUtil.isEmpty(DataKeyUtil.getDBKey(context, DB_VIBE_FLAG))) {
			builder.setDefaults(Notification.DEFAULT_VIBRATE);
		}

		if (mPushImage == null) {
			CLog.e("mPushImage is null");
		}

		// show notification
		builder.setStyle(new NotificationCompat.BigPictureStyle(builder).bigPicture(mPushImage).setBigContentTitle(pushMsg.notiTitle)
				.setSummaryText(pushMsg.notiMsg));

		int notificationId = getNewNotificationId();
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
		{
			if(FLAG_N.equals(ProPertiesFileUtil.getString(context, PRO_NOTI_GROUP_FLAG)))
			{
				notificationManager.cancelAll();
			}
			notificationManager.notify(notificationId, builder.build());
		}
		else
		{
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N && FLAG_Y.equals(ProPertiesFileUtil.getString(context, PRO_NOTI_GROUP_FLAG)))
			{
				notificationManager.cancelAll();
			}
			else
			{
				notificationManager.notify(notificationId, builder.build());
			}
		}
	}

	/**
	 * make pending intent
	 * 
	 * @param context
	 * @param extras
	 * @return
	 */
	private PendingIntent makePendingIntent(Context context, Bundle extras) {
		String activityAction = PMSUtil.getNotificationClickActivityAction(context);
		String activityClass = PMSUtil.getNotificationClickActivityClass(context);
		if(TextUtils.isEmpty(activityClass)){
			return clickListenerPendingIntent(context, extras);
		}

		Intent intent = null;
		try	{
			Class<?> cls = Class.forName(activityClass);
			intent = new Intent(context, cls);
		} catch (ClassNotFoundException e) {
			CLog.i("cannot found ("+(activityClass)+")");
		}

		if(intent == null){
			return null;
		}

		intent.putExtras(extras);
		intent.setAction(activityAction);

		if(PMSUtil.getNotificationClickActivityUseBackStack(context)) {
			TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
			stackBuilder.addNextIntentWithParentStack(intent);
			if (PMSUtil.getTargetVersion(context) >= 31) {
				return stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);
			} else {
				return stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
			}
		} else {
			if(PMSUtil.getNotificationClickActivityFlagEnable(context)) {
				try {
					if(PMSUtil.getNotificationClickActivityFlag(context) != null) {
						intent.setFlags(Integer.parseInt(PMSUtil.getNotificationClickActivityFlag(context)));
					}
				} catch (NumberFormatException numberFormatException) {
					numberFormatException.printStackTrace();
				}
			}
			if (PMSUtil.getTargetVersion(context) >= 31) {
				return PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);
			} else {
				return PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
			}
		}
	}

	private PendingIntent clickListenerPendingIntent(Context context, Bundle extras) {
		// notification
		Intent innerIntent = null;
		String receiverAction = null;
		String receiverClass = null;
		receiverAction = ProPertiesFileUtil.getString(context, PRO_NOTI_RECEIVER);
		receiverAction = receiverAction != null ? receiverAction : "com.pms.sdk.notification";
		receiverClass = ProPertiesFileUtil.getString(context, PRO_NOTI_RECEIVER_CLASS);

		if (receiverClass != null) {
			try {
				Class<?> cls = Class.forName(receiverClass);
				innerIntent = new Intent(context, cls).putExtras(extras);
				innerIntent.setAction(receiverAction);
			} catch (ClassNotFoundException e) {
				CLog.e(e.getMessage());
			}
		}
		if (innerIntent == null) {
			CLog.d("innerIntent == null");
			// setting push info to intent
			innerIntent = new Intent(receiverAction).putExtras(extras);
		}
		if (PMSUtil.getTargetVersion(context) >= 31) {
			return PendingIntent.getBroadcast(context, 0, innerIntent,
			                                  PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);
		} else {
			return PendingIntent.getBroadcast(context, 0, innerIntent, PendingIntent.FLAG_UPDATE_CURRENT);
		}
	}
	
	/**
	 * show popup (activity)
	 * 
	 * @param context
	 * @param extras
	 */
	private synchronized void showPopup (Context context, Bundle extras) {
		try {
			PushMsg pushMsg = new PushMsg(extras);

			if (PMSUtil.getNotiOrPopup(context) && pushMsg.msgType.equals("T")) {
				Toast.makeText(context, pushMsg.notiMsg, Toast.LENGTH_SHORT).show();
			} else {
				Class<?> pushPopupActivity;
				String pushPopupActivityName = ProPertiesFileUtil.getString(context, PRO_PUSH_POPUP_ACTIVITY);

				if (StringUtil.isEmpty(pushPopupActivityName)) {
					return;
				}

				try {
					pushPopupActivity = Class.forName(pushPopupActivityName);
				} catch (ClassNotFoundException e) {
					CLog.e(e.getMessage());
					pushPopupActivity = PushPopupActivity.class;
				}

				CLog.i("pushPopupActivity :" + pushPopupActivityName);

				Intent pushIntent = new Intent(context, pushPopupActivity);
				pushIntent.setFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION
						| Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
				pushIntent.putExtras(extras);

				if (PMSUtil.getPopupActivity(context)) {
					if (isOtherApp(context)) {
						context.startActivity(pushIntent);
					}
				} else {
					context.startActivity(pushIntent);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "deprecation", "static-access" })
	@SuppressLint("DefaultLocale")
	private boolean isOtherApp (Context context) {
		ActivityManager am = (ActivityManager) context.getSystemService(context.ACTIVITY_SERVICE);
		String topActivity = "";

		if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
			RunningAppProcessInfo currentInfo = null;
			Field field = null;
			try {
				field = RunningAppProcessInfo.class.getDeclaredField("processState");
			} catch (NoSuchFieldException e) {
				e.printStackTrace();
			}

			List<RunningAppProcessInfo> appList = am.getRunningAppProcesses();
			for (RunningAppProcessInfo app : appList) {
				if (app.importance == RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
					Integer state = null;
					try {
						state = field.getInt(app);
					} catch (IllegalAccessException e) {
						e.printStackTrace();
					} catch (IllegalArgumentException e) {
						e.printStackTrace();
					}
					if (state != null && state == START_TASK_TO_FRONT) {
						currentInfo = app;
						break;
					}
				}
			}
			topActivity = currentInfo.processName;
		} else {
			List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(10);
			topActivity = taskInfo.get(0).topActivity.getPackageName();
		}

		CLog.e("TOP Activity : " + topActivity);
		if (USE.equals(ProPertiesFileUtil.getString(context, PRO_PUSH_POPUP_SHOWINT_FLAG))) {
			if ((topActivity.toLowerCase().indexOf("launcher") != -1) // 런처
					|| (topActivity.toLowerCase().indexOf("locker") != -1) // 락커
					|| topActivity.equals("com.google.android.googlequicksearchbox") // 넥서스 5
					|| topActivity.equals("com.cashslide") // 캐시 슬라이드
					|| topActivity.equals("com.kakao.home") // 카카오런처
					|| topActivity.equals(context.getPackageName())) {
				return true;
			}
		} else {
			if (topActivity.equals(context.getPackageName())) {
				return true;
			}
		}

		return false;
	}

	/**
	 * is image push
	 * 
	 * @param extras
	 * @return
	 */
	private boolean isImagePush (Bundle extras) {
		try {
			if (!PhoneState.isNotificationNewStyle()) {
				throw new Exception("wrong os version");
			}
			String notiImg = extras.getString(KEY_NOTI_IMG);
			CLog.i("notiImg:" + notiImg);
			if (notiImg == null || "".equals(notiImg) || (notiImg.indexOf("http") == -1)) {
				throw new Exception("no image type");
			}
			return true;
		} catch (Exception e) {
			CLog.e("isImagePush:" + e.getMessage());
			return false;
		}
	}

	@TargetApi(Build.VERSION_CODES.O)
	private String createNotiChannel(Context context, NotificationManager notificationManager, String strNotiChannel)
	{
		NotificationChannel notiChannel = notificationManager.getNotificationChannel(strNotiChannel);
		boolean isShowBadge = false;
		boolean isPlaySound = false;
		boolean isPlaySoundChanged = false;
		boolean isEnableVibe;
		boolean isShowBadgeOnChannel;
		boolean isPlaySoundOnChannel;
		boolean isEnableVibeOnChannel;
		boolean isGroupOnChannel;
		boolean isAlarmVolumeZero;
		boolean isGroup;

		if (IPMSConsts.FLAG_Y.equals(ProPertiesFileUtil.getString(context, PRO_NOTI_O_BADGE)))
		{
			isShowBadge = true;
		} else
		{
			isShowBadge = false;
		}
		if (IPMSConsts.FLAG_Y.equals(DataKeyUtil.getDBKey(context, DB_VIBE_FLAG)))
		{
			isEnableVibe = true;
		} else
		{
			isEnableVibe = false;
		}
		if(IPMSConsts.FLAG_Y.equals(ProPertiesFileUtil.getString(context, PRO_NOTI_GROUP_FLAG)))
		{
			isGroup = true;
		}
		else
		{
			isGroup = false;
		}
		if(FLAG_Y.equals(DataKeyUtil.getDBKey(context, DB_RING_FLAG)))
		{
			isPlaySound = true;
		}
		else
		{
			isPlaySound = false;
		}
		CLog.d("AppSetting isShowBadge " + isShowBadge +
					   " isPlaySound " + isPlaySound +
					   " isEnableVibe " + isEnableVibe);

		if (notiChannel == null)
		{    //if notichannel is not initialized
			CLog.d("notification channel is initialized");
			notiChannel = new NotificationChannel(strNotiChannel, PMSUtil.getApplicationName(context), NotificationManager.IMPORTANCE_HIGH);
			notiChannel.setShowBadge(isShowBadge);
			notiChannel.enableVibration(isEnableVibe);
			notiChannel.setLightColor(Notification.FLAG_SHOW_LIGHTS);
			notiChannel.setImportance(NotificationManager.IMPORTANCE_HIGH);
			notiChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
			if (isEnableVibe)
			{
				notiChannel.setVibrationPattern(new long[]{1000, 1000});
			}

			if (isPlaySound)
			{
				Uri uri;
				try
				{
					int notiSound = PMSUtil.getNotiSound(context);
					if (notiSound > 0)
					{
						uri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + context.getPackageName() + "/" + notiSound);
						CLog.d("notiSound " + notiSound + " uri " + uri.toString());
					} else
					{
						throw new Exception("default ringtone is set");
					}
				}
				catch (Exception e)
				{
					uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//					CLog.e(e.getMessage());
				}
				AudioAttributes audioAttributes = new AudioAttributes.Builder()
						.setUsage(AudioAttributes.USAGE_NOTIFICATION)
						.build();
				notiChannel.setSound(uri, audioAttributes);
				CLog.d("setChannelSound ring with initialize notichannel");
			} else
			{
				notiChannel.setSound(null, null);
				CLog.d("setChannelSound muted with initialize notichannel");
			}
			notificationManager.createNotificationChannel(notiChannel);
			return strNotiChannel;
		} else
		{
			CLog.d("notification channel is exist");
			return strNotiChannel;
		}
	}

	/**
	 * finish runnable
	 */
	private final Runnable finishRunnable = new Runnable() {
		@Override
		public void run () {
			if (wl != null && wl.isHeld()) {
				wl.release();
			}
		};
	};
	public int getNewNotificationId () {
		int notificationId = sNotificationId++;

		// Unlikely in the sample, but the int will overflow if used enough so we skip the summary
		// ID. Most apps will prefer a more deterministic way of identifying an ID such as hashing
		// the content of the notification.
		if (notificationId == NOTIFICATION_GROUP_SUMMARY_ID) {
			notificationId = sNotificationId++;
		}
		return notificationId;
	}

	public static int getNotificationId () {
		return sNotificationId;
	}
}
